package com.zzc.springbootmybatis.mapper;

import com.zzc.springbootmybatis.bean.Employee;

public interface EmployeeMapper {

     Employee getEmployeeById(Integer id);

     void insertEmp(Employee employee);



}
