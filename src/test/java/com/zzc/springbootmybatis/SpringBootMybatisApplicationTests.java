package com.zzc.springbootmybatis;

import com.zzc.springbootmybatis.bean.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringBootMybatisApplicationTests {

    @Test
    void contextLoads() {
        System.out.println(new Employee().toString());
    }

}
