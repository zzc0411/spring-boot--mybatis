## Springboot与Mybatis



### 1.整合Mybatis

#### 1pom.xml文件配置

```xml
 		<dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>2.1.4</version>
        </dependency>
```

使用lombok

```xml
 <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.6</version>
            <scope>provided</scope>
   </dependency>
```



#### 2.创建数据对象

![image-20201211172804345](C:\Users\10729\AppData\Roaming\Typora\typora-user-images\image-20201211172804345.png)



#### 3.创建配置类

##### 1.开启驼峰大小写



<img src="C:\Users\10729\AppData\Roaming\Typora\typora-user-images\image-20201211172900305.png" alt="image-20201211172900305" style="zoom:200%;" />



```java
@org.springframework.context.annotation.Configuration
public class MyBatisConfig {
    @Bean
    public ConfigurationCustomizer configurationCustomizer(){
       return new ConfigurationCustomizer(){

            @Override
            public void customize(Configuration configuration) {
                configuration.setMapUnderscoreToCamelCase(true);
            }
        };
    }
}

```



##### 2.Druid配置类

```java
@Configuration
public class DruidConfig {
    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource druid(){
        return  new DruidDataSource();
    }

    //配置Druid的监控
    //1、配置一个管理后台的Servlet
    @Bean
    public ServletRegistrationBean statViewServlet(){
        ServletRegistrationBean bean = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
        Map<String,String> initParams = new HashMap<>();

        initParams.put("loginUsername","admin");
        initParams.put("loginPassword","123456");
        initParams.put("allow","");//默认就是允许所有访问
        initParams.put("deny","192.168.15.21");

        bean.setInitParameters(initParams);
        return bean;
    }


    //2、配置一个web监控的filter
    @Bean
    public FilterRegistrationBean webStatFilter(){
        FilterRegistrationBean bean = new FilterRegistrationBean();
        bean.setFilter(new WebStatFilter());

        Map<String,String> initParams = new HashMap<>();
        initParams.put("exclusions","*.js,*.css,/druid/*");

        bean.setInitParameters(initParams);

        bean.setUrlPatterns(Arrays.asList("/*"));

        return  bean;
    }

}
```



##### 3.数据库配置文件



![image-20201211173110550](C:\Users\10729\AppData\Roaming\Typora\typora-user-images\image-20201211173110550.png)





```yml
spring:
  datasource:
    username: root
    password: 123456
    url: jdbc:mysql://localhost:3306/mybatis?serverTimezone=UTC
    driver-class-name: com.mysql.cj.jdbc.Driver
    type: com.alibaba.druid.pool.DruidDataSource
    initialization-mode: always

mybatis:
  config-location: classpath:mybatis/mybatis-config.xml
  mapper-locations: classpath:mybatis/mapper/*.xml
#      map-underscore-to-camel-case:true
```



#### 4.配置映射接口

##### 1.全注解方式

![image-20201211173304498](C:\Users\10729\AppData\Roaming\Typora\typora-user-images\image-20201211173304498.png)



##### 2.XML配置

###### 1.接口配置

![image-20201211173351844](C:\Users\10729\AppData\Roaming\Typora\typora-user-images\image-20201211173351844.png)



###### 2.XML配置SQL语句

![image-20201211173417156](C:\Users\10729\AppData\Roaming\Typora\typora-user-images\image-20201211173417156.png)

###### 3.XML配置Mybatis设置

（开启驼峰识别）

![image-20201211173427844](C:\Users\10729\AppData\Roaming\Typora\typora-user-images\image-20201211173427844.png)



###### 4.YML文件配置注册XML文件位置

![image-20201211173654517](C:\Users\10729\AppData\Roaming\Typora\typora-user-images\image-20201211173654517.png)





#### 5.编写控制器



![image-20201211173851285](C:\Users\10729\AppData\Roaming\Typora\typora-user-images\image-20201211173851285.png)